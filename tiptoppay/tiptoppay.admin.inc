<?php

/**
 * Payment method callback: settings form.
 */
function tiptoppay_admin_settings() {
	$form = array();
	
	$aStatus = commerce_order_status_options_list();
	$form['tiptoppay_public_id'] = array(
		'#type' => 'textfield',
		'#title' => 'Public ID',
		'#default_value' => variable_get('tiptoppay_public_id',''),
	);

	$form['tiptoppay_api_password'] = array(
		'#type' => 'textfield',
		'#title' => 'Password for API',
		'#default_value' => variable_get('tiptoppay_api_password',''),
	);

	$form['tiptoppay_scheme'] = array(
		'#type' => 'select',
		'#title' => t('Payment scheme'),
		'#default_value' => variable_get('tiptoppay_scheme','sms'),
		'#options' => array(
			'sms' => 'SMS',
			'dms' => 'DMS',
		),
	);
    
    $form['tiptoppay_skin'] = array(
		'#type' => 'select',
		'#title' => t('Payment skin'),
		'#default_value' => variable_get('tiptoppay_skin','classic'),
		'#options' => array(
			'classic' => 'Classic',
			'modern' => 'Modern',
			'mini' => 'Mini',
		),
	);
    
	$form['tiptoppay_send_check'] = array(
		'#type' => 'select',
		'#title' => t('Send receipt'),
		'#default_value' => variable_get('tiptoppay_send_check','yes'),
		'#options' => array(
			'yes' => 'Да',
			'no' => 'Нет',
		),
	);

	$form['tiptoppay_localization'] = array(
		'#type' => 'select',
		'#title' => t('Widget localization'),
		'#default_value' => variable_get('tiptoppay_localization','ru-RU'),
		'#options' => array(
			"ru-RU"=> 'Русский',
			"en-US"=>'Английский',
			"lv"=>'Латышский',
			"az"=>'Азербайджанский',
			"kk-KZ"=>'Казахский',
			"uk"=>'Украинский',
			"pl"=>'Польский',
			"pt"=>'Португальский',
		),
	);

	$form['tiptoppay_tax_system'] = array(
		'#type' => 'select',
		'#title' => t('Taxation system'),
		'#default_value' => variable_get('tiptoppay_tax_system',0),
		'#options' => array(
			'0' => t('0 — General tax regime'),
			'1' => t('1 — CPR based on simplified declaration'),
			'4' => t('4 — CPR based on patent'),
		)
	);

	$form['tiptoppay_tax_product'] = array(
		'#type' => 'select',
		'#title' => t('Product tax'),
		'#default_value' => variable_get('tiptoppay_tax_product',0),
		'#options' => array(
			'-1' => 'VAT Free',
			'0' => 'VAT 0%',
			'10' => 'VAT 10%',
			'12' => 'VAT 12%',
			'-2' => t('Take from product')
		)
	);


	$form['tiptoppay_tax_shipping'] = array(
		'#type' => 'select',
		'#title' => t('Shipping tax'),
		'#default_value' => variable_get('tiptoppay_tax_shipping',0),
		'#options' => array(
			'-1' => 'VAT Free',
			'0' => 'VAT 0%',
			'10' => 'VAT 10%',
			'12' => 'VAT 12%',
			'-2' => t('Take from product')
		)
	);
	
	$form['tiptoppay_status_success'] = array(
		'#type' => 'select',
		'#title' => t('Order payed successfully'),
		'#default_value' => variable_get('tiptoppay_status_success',''),
		'#options' => $aStatus,
	);
	
	$form['tiptoppay_status_pending'] = array(
		'#type' => 'select',
		'#title' => t('The status of the new (unpaid) order'),
		'#default_value' => variable_get('tiptoppay_status_pending',''),
		'#options' => $aStatus,
	);

	$form['tiptoppay_status_ttp_authorized'] = array(
		'#type' => 'select',
		'#title' => t('Authorized payment status (DMS)'),
		'#default_value' => variable_get('tiptoppay_status_ttp_authorized',''),
		'#options' => $aStatus,
	);

	$form['tiptoppay_status_ttp_confirmed'] = array(
		'#type' => 'select',
		'#title' => t('Confirmed payment status (DMS)'),
		'#default_value' => variable_get('tiptoppay_status_ttp_confirmed',''),
		'#options' => $aStatus,
	);

	$form['tiptoppay_status_canceled'] = array(
		'#type' => 'select',
		'#title' => t('Order cancelled (DMS)'),
		'#default_value' => variable_get('tiptoppay_status_canceled',''),
		'#options' => $aStatus,
	);
	
	$form['tiptoppay_status_refund'] = array(
		'#type' => 'select',
		'#title' => t('Refund order'),
		'#default_value' => variable_get('tiptoppay_status_refund',''),
		'#options' => $aStatus,
	);
	
	$form_state = array();
	$fields = array();
	$profile = commerce_customer_profile_new('billing');
	field_attach_form('commerce_customer_profile', $profile, $fields, $form_state);
	$aField = ['' => '---'];
	foreach($form_state['field'] as $sKey => $mValue) {
		$aField[$sKey] = $sKey;
	}
	drupal_get_messages('error'); // получим ошибки, чтобы их очистить и не выводить пользователю.

	$form['tiptoppay_field_phone'] = array(
		'#type' => 'select',
		'#title' => t('Field with phone'),
		'#default_value' => variable_get('tiptoppay_field_phone',''),
		'#options' => $aField,
	);
	
	return system_settings_form($form);
}

function tiptoppay_admin_settings_validate($form, &$form_state) {

}